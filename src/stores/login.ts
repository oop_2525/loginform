import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", () => {
  const LoginName = ref("");
  const isLogin = computed(() => {
    // loginname is not empty
    return LoginName.value !== "";
  });
  const login = (username: string): void => {
    LoginName.value = username;
    localStorage.setItem("loginname", username);
  };
  const logout = () => {
    LoginName.value = "";
    localStorage.removeItem("loginname");
  };
  const loadData = () => {
    LoginName.value = localStorage.getItem("loginname") || "";
  };

  return { LoginName, isLogin, login, logout, loadData };
});
